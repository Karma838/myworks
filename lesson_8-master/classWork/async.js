/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
/*
var getCompanyUrl = "http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2";
// 1. Создаём новый объект XMLHttpRequest

// Нас интересует 3 параметра: xhr.status, xhr.statusText, xhr.responseText
// 4. Если код ответа сервера не 200, то это скорее всего ошибка
// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
if (xhr.status !== 200) {
    // Обработаем ошибку
    console.log( xhr.status, xhr.statusText ); // пример вывода: 404: Not Found
} else {
    // вывести результат
    var myResponse = JSON.parse(xhr.responseText);
    console.log(xhr, myResponse); // responseText -- текст ответа.
    myResponse.map(item => console.log(item.company))
}
*/
window.onload =()=> {

    var getCompanyUrl = "http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2";


    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    function GetData(url) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, false);
            xhr.send();

            if (xhr.status !== 200) {
                reject(xhr.status, xhr.statusText);
            } else {

                var myResponse = JSON.parse(xhr.responseText);
                console.log("MyResponse: ", myResponse);
                resolve(myResponse);

            }

        })
    }

    async function FillTable() {
        await GetData(getCompanyUrl)
            .then((resolve) => {
                const objArray = [];
                const selectedCompanies = resolve;
                selectedCompanies.map(item => {
                    let {company, balance, date, address} = item;

                    const newCompany = {
                        company,
                        balance,
                        date,
                        address
                    };
                    newCompany.date = randomDate(new Date(2010, 0, 1),new Date());
                    objArray.push(newCompany);
                    console.log(newCompany);
                })
                return objArray;


            }, (reject) => {
                console.log(reject);
            }).then(
                (resolve) => {
                    let id = 0;
                    resolve.map(item => {

                        console.log(item);
                        id++;
                        item.id = id;
                        let ul = document.createElement("ul");
                        let AsyncD = document.querySelector(".Async");
                        // console.log("Ul:", ul);
                        //  console.log("Div:", AsyncD);
                        let li = document.createElement("li");
                        li.innerHTML = `
<span>${item.company}</span> |
<span >${item.balance}</span> |
<button id="date${item.id}">show date</button>
<span hidden="hidden" class="date">${item.date.getDate()}/${item.date.getMonth()}/${item.date.getFullYear()}</span> |
<button id="address${item.id}">show address</button>
<span hidden="hidden" class="address">${item.address.street} Str. , ${item.address.city} City , ${item.address.state} State , ${item.address.country}</span>`;
                        ul.appendChild(li);
                        AsyncD.appendChild(ul);

                        let btn = document.getElementById(`date${item.id}`);
                           btn.addEventListener("click",(e)=>{
                               e.preventDefault();
                              let span = li.querySelector(".date");
                              span.removeAttribute("hidden");
                              btn.setAttribute("hidden","hidden")
                           })
                           let btn2 = document.getElementById(`address${item.id}`);
                           btn2.addEventListener("click",(e)=>{
                               e.preventDefault();
                               let span = li.querySelector(".address");
                               span.removeAttribute("hidden");
                               btn2.setAttribute("hidden","hidden")
                           })



                    });


                }
            )


    }

    console.log(FillTable());


}