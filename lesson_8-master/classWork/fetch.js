/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

url = " http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";


  fetch(url)
    .then( data=>{
        let users = data.json();
        console.log("Users: ",users);
      return users;
    })
    .then((users) =>{
        const selectedUser  =  users[5];
        console.log("User: ",selectedUser);
        return selectedUser;
    })
    .then( (selectedUser) => {
        return fetch(url)
            .then((data)=>{
                const userFriends = data.json();
                return userFriends;
            })
            .then((data)=>{
                const CombinedUser = {
                    age:selectedUser.age,
                    name:selectedUser.name,
                    gender:selectedUser.gender,
                    friends: data[5].friends
                };
                return CombinedUser;
        })
    })
    .then( (user) =>{

        let ul = document.createElement("ul");
        let subUl = document.createElement("ul");
        let li = document.createElement("li");

        li.innerHTML =`
        Person's name is ${user.name} . He/She is ${user.age} years old . Gender is   ${user.gender} and friends:`

        user.friends.map(item =>{
            let subLi = document.createElement("li");
            subLi.innerHTML = `${item.name}`;
            subUl.appendChild(subLi);
        });

        li.appendChild(subUl);
        ul.appendChild(li);
        document.getElementById("Fetch").appendChild(ul);
    })
