const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
/*
      Задание:
      1. При помощи методов изложеных ниже, переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
      2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
          + Бонусный бал. Вывести на страничку списком
      3. Реализация функции поиска по массиву ITEA_COURSES.
          + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.
    */
window.onload = ()=> {
    var arrOfLength = [];
    ITEA_COURSES.forEach((item) => {
        arrOfLength[item] = item.length;
    })
    console.log(arrOfLength);
    ITEA_COURSES.sort();
    let list = document.createElement("ul");

    ITEA_COURSES.forEach((item) => {
        let inlist = document.createElement("li");
        inlist.innerText = item;
        list.appendChild(inlist);

    })
    document.getElementById("Task1").appendChild(list);
  //  console.log(ITEA_COURSES);



    var btn = document.getElementById("prss");
    btn.addEventListener("click",()=>{
        let inpVal = document.getElementById("inpt").value;


        const StringLength = item => item.includes(inpVal);
        // // // if expresion === true return item
         var filtredArray = ITEA_COURSES.filter(StringLength);
         console.log('filtredArray:', filtredArray);
         console.log('Array:', ITEA_COURSES);
        let list = document.createElement("ul");

        filtredArray.forEach((item) => {
            let inlist = document.createElement("li");
            inlist.innerText = item;
            list.appendChild(inlist);
        })
        document.getElementById("Task2").appendChild(list);

    })








/*
  Задание 2:
  Написать фильтр массива по:
  Актеру, продолжительности

  Бонус:
    Сделать графическую оболочку для программы:
    - Селект со списком актеров
    - Селект или инпут с продолжительностью
    - Результат в виде списка фильмов
*/

    var stars = ['Elijah Wood', 'Ian McKellen', 'Orlando Bloom','Ewan McGregor',' Liam Neeson', 'Natalie Portman', 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
    var movies = [
        {
            name: "Lord of the Rings",
            duration: 240,
            starring: [ 'Elijah Wood', 'Ian McKellen', 'Orlando Bloom']
        },
        {
            name: "Star Wars: Episode I - The Phantom Menace",
            duration: 136,
            starring: [ 'Ewan McGregor',' Liam Neeson', 'Natalie Portman']
        },
        {
            name: "Fargo",
            duration: 100,
            starring: [ 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
        },
        {
            name: "V for Vendetta",
            duration: 150,
            starring: [ 'Hugo Weaving', 'Natalie Portman', 'Rupert Graves']
        }
    ];


    var actorSelector = document.getElementById("selector");
    stars.forEach(function(star){
        let option = new Option(star);
        option.id = star;
        actorSelector.appendChild(option);
    });
    console.log(actorSelector);
    let ul = document.getElementById("filmList");
    actorSelector.addEventListener('change', function() {
        ul.innerHTML = "";
        actor = this.value;
        console.log(actor);

        movies.forEach((item)=>{
            if(item.starring.indexOf(actor) !== -1) {

                let li = document.createElement("li");
                li.innerText = item.name + " длительность: " + item.duration;
                ul.appendChild(li);

            }
        });

    })

    let button = document.getElementById("press");
    let inp = document.getElementById("duration");
    button.addEventListener('click', function() {
        ul.innerHTML = "";
        movies.forEach((item)=>{
            if(item.duration <= inp.value) {
                let li = document.createElement("li");
                li.innerText = item.name + " длительность: " + item.duration;
                ul.appendChild(li);
            }
        });

    })


}
