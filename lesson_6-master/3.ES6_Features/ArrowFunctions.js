/*
  Arrow functions + default values
  Docs: https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Functions/Arrow_functions
*/
    // const myFuncName = param => console.log( param );

    // const MyArrowFunction = ( a, b, c ) => {
    //   console.log( a, b ,c );
    // }
    //
    // MyArrowFunction(1,2,3);

    // const myShortFunc = param => console.log( param ); // return expresion
    // myShortFunc('hello!');

    // const y = ( param, param2 ) => {
    //   console.log( param, param2 );
    //   return 'hello';
    // };
    //
    // let test = y(1, 2);
    // console.log( test );

    // const shortFunc = (param, param2) => param + param2;
    // const o = function( param, param2 ){
    //   // console.log( param, param2 );
    //   return param + param2;
    // };

    // const Duble = a => a * 2;
    // console.log( Duble(4) );

    // const Test = (one, two) => {
    //   console.log( one, two );
    //   return one + two;
    // }

    // console.log( Test(10, 5) );
