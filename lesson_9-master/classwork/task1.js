/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import {swim} from './modules/Swim';
import {fly} from './modules/Fly';
import {run} from './modules/Run';
import {eat} from './modules/Eat';

function  constrPigeon(name){
    this.name = name,
    this.fly = fly,
        this.eat = eat

}
function constrPinguin(name){
        this.name = name,
    this.swim = swim,
    this.run = run
}

let pigeon = new constrPigeon("vasya");
pigeon.eat();
pigeon.fly();

let pinguin = new constrPinguin("Petya");
pinguin.swim();
pinguin.run();
