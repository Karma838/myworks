





window.onload =()=> {


    var messageBox = document.getElementById("message");
    var select = document.getElementById("author");
    var messageList = document.getElementById("message_list");
    var author = "";
    var text = "";
    var date = "";
    var childId = 0;
    let message = document.getElementById("message");
    let send = document.getElementById("submit");
    let commentArray = [];
    var id = 0;

    function getAuthor() {

        select.addEventListener("change",(event) => {
            author = event.target.value;
            console.log(author);

        })

    }

    function getText() {
        let msg = document.getElementById("message");
        text = msg.value;
    }


    function setDate() {
        let dte = new Date();
        date = dte;
    }

    function chooseText() {
        let txt = document.getElementsByName("myRadio");
//    console.log(txt);
        txt.forEach((item)=>{
            item.addEventListener("change", () => {
                messageBox.value = item.dataset.text;

                getText();
            })
        })


    }







    function createMessageFeed(){
        messageList.innerHTML = "";
        commentArray.forEach((item) => {
            let li = document.createElement("li");
            li.dataset.id = item.id;
            li.innerHTML = `<div class="message__date">
                        ${item.date}
                      </div>
                      <div class="message__author">
                        <b>${item.author}</b>
                      </div>
                      <div class="message__text">
                        ${item.text}
                      </div>
                      <div class="message__controls" data-id="${item.id}">
                        <button class="_skipMessage" >Skip</button>
                        <button class="_answerMessage">Answer</button>
                      </div>`;
            const skipBtn = li.querySelector('._skipMessage');
            const answBtn = li.querySelector('._answerMessage');
            skipBtn.addEventListener('click', item.SkipMessage )
            answBtn.addEventListener('click', item.AnswerMessage);
            messageList.appendChild(li);

            //Делаем проверку, если есть ответы - рендерим их тоже.
            if (item.answers.length !== 0){
                item.answers.forEach(item => {
                    const ul = document.createElement('ul');
                    const subLi = document.createElement('li');
                    subLi.innerHTML = `<div class="message__date">
                              ${item.date}
                            </div>
                            <div class="message__author">
                              <b>${item.author}</b>
                            </div>
                            <div class="message__text">
                              ${item.text}
                            </div>
                            <div class="message__controls" data-id="${item.id}">
                            </div>`
                    li.appendChild(ul);
                    ul.appendChild(subLi);
                })
            }
        })
    }

    chooseText();
    getAuthor();
    setDate();

    //Класс сообщений
    class Message {
        constructor(id, author, text, date, answers){
            this.id = id;
            this.author = author;
            this.text = text;
            this.date = new Date();
            this.SkipMessage = this.SkipMessage.bind(this);
            this.AnswerMessage = this.AnswerMessage.bind(this);
            this.SendAnswer = this.SendAnswer.bind(this);
            this.answers = [];
        }

        SkipMessage(){
            commentArray.forEach((item) => {
                if (item.id === this.id){
                    let position = commentArray.indexOf(this);
                    commentArray.splice(position,1);
                    createMessageFeed()
                }
            });
        }

        AnswerMessage(event){
            const divWrap = document.createElement('div');
            const divContr = document.createElement('div');
            const textArea = document.createElement('textarea');
            textArea.id = 'answerFor' + this.id;
            const btn = document.createElement('button');
            btn.innerText = 'Answer';
            btn.addEventListener('click', this.SendAnswer);
            const li = document.querySelector('li[data-id="' + this.id + '"]');
            li.appendChild(divWrap);
            divWrap.appendChild(textArea)
            li.appendChild(divContr);
            divContr.appendChild(btn);
            event.target.setAttribute('disabled', true);
        }

        SendAnswer (){
            childId++
            class Answer extends Message {
                constructor (id, author, text, date, answers, parentId){
                    super(id, author, text, date, answers);
                    this.parentId = this.id;
                }
            }

            const textField = document.getElementById('answerFor' + this.id);
            let date = new Date()
            let parentId = this.id;
            let answer = new Answer (childId, author, textField.value, date, [], parentId);
            let index = commentArray.indexOf(this);
            commentArray[index].answers.push(answer);
            const li = document.querySelector('li[data-id="' + this.id + '"]');
            const ul = document.createElement('ul');
            const subLi = document.createElement('li');
            subLi.innerHTML = `<div class="message__date">
                        ${answer.date}
                      </div>
                      <div class="message__author">
                        <b>${answer.author}</b>
                      </div>
                      <div class="message__text">
                        ${answer.text}
                      </div>`
            li.appendChild(ul);
            ul.appendChild(subLi);
        }
    }

    send.addEventListener('click', event=>{
        id++
        event.preventDefault();
        let msg = new Message(id, author, message.value);
        commentArray.push(msg);
        createMessageFeed()
    })

}
